package test;
import com.google.common.annotations.VisibleForTesting;
import main.SearchProductByString;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;


public class AmazonTests
{
    /**
     * Verifies that the driver has navigated to the correct website
     */
    @Test
    public void checkIfCorrectSite()
    {
        String url = "https://www.amazon.com";
        String expectedTitle = "Amazon.com: Online Shopping for Electronics, Apparel, Computers, Books, DVDs & more";
        SearchProductByString searchProduct = new SearchProductByString();
        WebDriver driver = searchProduct.setUpChromeDriver(url);

        try
        {
            Assert.assertEquals(expectedTitle, driver.getTitle());
            System.out.println("Navigated to correct web page.");
        }
        catch(Throwable pageNavigationError)
        {
            System.out.println("Didn't navigate to correct web page");

        }

        driver.close();
    }

    /**
     * Verifies that the search bar on Amazon returns a list of Nikon items
     */
    @Test
    public void checkSearchBarReturnsNikonItems()
    {
        String url = "https://www.amazon.com";
        String searchTerm = "Nikon";
        SearchProductByString searchProduct = new SearchProductByString();
        WebDriver driver = searchProduct.setUpChromeDriver(url);
        searchProduct.searchProductByString(driver);

        try
        {
            String actualResult = driver.findElement(By.xpath("//span[@class='a-color-state a-text-bold'")).getText();
            Assert.assertEquals(searchTerm, actualResult);

        }
        catch(Exception e)
        {
            System.out.println("Term does not exist");
        }

        driver.close();
   }
}
