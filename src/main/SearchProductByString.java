package main;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class SearchProductByString
{
    /**
     * This sets up the WebDriver that will be used to open the Chrome browser.
     * It takes the url of the website you want to test as a parameter.
     * @param url
     * @return
     */
    public static WebDriver setUpChromeDriver(String url)
    {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\Selenium\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
        driver.get(url);

        return driver;
    }

    /**
     * This enters in a specified string of text (Nikon) into a search bar.
     * It then presses the Enter key to return a list of results.
     * It takes the WebDriver you want to use as a parameter.
     * @param driver
     */
    public void searchProductByString(WebDriver driver)
    {
        driver.findElement(By.xpath("//input[@id='twotabsearchtextbox']")).sendKeys(("Nikon"));
        driver.findElement(By.xpath("//input[@id='twotabsearchtextbox']")).sendKeys(Keys.ENTER);
    }


}
