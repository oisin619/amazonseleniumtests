package main;

import org.openqa.selenium.WebDriver;

public class Main
{
    public static void main(String args[]) throws InterruptedException
    {
        SearchProductByString searchProduct = new SearchProductByString();
        //Testing to see if the Selenium packages are installed correctly.
        WebDriver driver = searchProduct.setUpChromeDriver("https://www.amazon.com");
        searchProduct.searchProductByString(driver);
    }
}
